import sys

# Insert the directory of sdatabase.py:
DBPATH = r'C:\Users\Meir\Documents\CS\Projects\barak-kolnik\ipc'
sys.path.insert(0, DBPATH)

from sdatabase import DatabaseManager


class TestDatabaseManager:
    def __init__(self, db):
        assert isinstance(db, DatabaseManager)
        self.db = db

    def begin_reading(self, amount=1):
        assert amount <= self.db._p.rlimit

        for i in xrange(amount):
            self.db._p.start_read()

    def end_reading(self, amount=1):
        used = amount % self.db._p.rlimit # the amount can't be longer than the readers limit.
        for i in xrange(used):
            self.db._p.end_read()

    def begin_writing(self):
        self.db._p.start_write()

    def end_writing(self):
        self.db._p.end_write()

    def can_read(self):
        return self.db._p.can_read()

    def can_write(self):
        return self.db._p.can_write()


def test_single_threaded_read():
    m = TestDatabaseManager(DatabaseManager(DBPATH + "\\a", {"1": 1}, 10))
    assert m.can_read()      # easy reading no competition


def test_single_threaded_write():
    m = TestDatabaseManager(DatabaseManager(DBPATH + "\\a", {"1": 1}, 10))
    assert m.can_write()     # easy writing no competition


def test_cant_write():
    m = TestDatabaseManager(DatabaseManager(DBPATH + "\\a", {"1": 1}, 10))

    m.begin_reading()
    assert not m.can_write()     # can't write when someone is reading
    m.end_reading()


def test_cant_read():
    m = TestDatabaseManager(DatabaseManager(DBPATH + "\\a", {"1": 1}, 10))
    m.begin_writing()
    assert not m.can_read()      # can't read when someone is writing
    m.end_writing()


def test_multiple_readers():
    m = TestDatabaseManager(DatabaseManager(DBPATH + "\\a", {"1": 1}, 10))

    m.begin_reading()
    assert m.can_read()  # can read when there is 1 reader.
    m.begin_reading(4)
    assert m.can_read()  # can read when there are 5 readers.
    m.end_reading(5)


def test_readers_and_writer():
    m = TestDatabaseManager(DatabaseManager(DBPATH + "\\a", {"1": 1}, 10))

    m.begin_reading()
    assert m.can_read()          # can read when there is 1 reader
    assert not m.can_write()     # can't write when there is 1 reader
    m.begin_reading()
    assert m.can_read()          # can read when there are 2 readers
    assert not m.can_write()     # can't write when there are 2 readers
    m.end_reading(2)

    # 0 readers/writers

    assert m.can_write()         # can write when there no one is reading
    m.begin_writing()
    assert not m.can_read()  # can't read when someone is writing
    m.end_writing()

    # 0 readers/writers again

    assert m.can_read()  # can read when no one is writing
