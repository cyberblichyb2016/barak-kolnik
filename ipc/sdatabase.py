import pickle
import threading

# Author: Barak Kolnik


class Database:
    """ Simple database class"""
    def __init__(self, filename, obj):

        self.filename = filename
        self.origin = obj

    def create_db(self):
        with open(self.filename, "w+") as newfile:
            pickle.dump(self.origin, newfile)

    def read(self):
        """ Get database content"""
        with open(self.filename, "r") as db:
            return pickle.load(db)

    def write(self, new):
        """ re-write the database content"""
        with open(self.filename, "w+") as db:
            pickle.dump(new, db)


class Permissions:
    """ A class for handling the permissions logic for a database"""
    def __init__(self, rlimit):
        self.rlimit = rlimit
        self.rlock = threading.Semaphore(rlimit) # main lock for database, used by readers and writers
        self.wlock = threading.Lock()


    def can_read(self):
        """ Returns if the database could be read when the function was called. """
        available = self.rlock.acquire(False)
        if available:
            self.rlock.release()
            return True
        return False

    def slots_available(self):
        """ Returns the amount of available slots for readers in the database when the function was called."""
        slots_available = 0
        for i in xrange(self.rlimit):
            if self.rlock.acquire(False):
                slots_available += 1
            else:
                break

        # print "({})".format(slots_available)

        for i in xrange(slots_available):
            self.rlock.release()

        return slots_available

    def can_write(self):
        """ Returns if the database could be written when the function was called. """
        if self.slots_available() == self.rlimit:
            if self.wlock.acquire(False):
                self.wlock.release()
                return True
        return False

    def acquire_whole_rlock(self):
        """ Acquires all of the slots for the database """
        for i in xrange(self.rlimit):
            self.rlock.acquire()

    def release_whole_rlock(self):
        """
        Releases all of the slots for the database.
        It is presumed that the caller has all of the available slots in the first place.
        """
        for i in xrange(self.rlimit):
            self.rlock.release()

    def start_read(self):
        """ Handles the procedures need to be made to enable reading. """
        self.rlock.acquire()

    def end_read(self):
        """ Handles the procedures need to be made when the reading is done. """
        self.rlock.release()

    def start_write(self):
        """ Handles the procedures need to be made to enable writing. """
        self.wlock.acquire()
        self.acquire_whole_rlock()

    def end_write(self):
        """ Handles the procedures need to be made when the writing is done. """
        self.release_whole_rlock()
        self.wlock.release()


class DatabaseManager:
    """ A class for managing calls for a database. """
    def __init__(self, filename, obj, rlimit):
        self._p = Permissions(rlimit)
        self.db = Database(filename, obj)
        self.db.create_db()

    def read(self):
        """ Returns the content of the database."""
        self._p.start_read()
        result = self.db.read()
        self._p.end_read()
        return result

    def write(self, new):
        """ Sets the database to be equal to 'new'. """
        self._p.start_write()
        self.db.write(new)
        self._p.end_write()
        return



